#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#ifndef STASSID
#define STASSID "gostriksh"
#define STAPSK  "test1234"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(3080);

const int buzzer = D8;

void preparePin() {
  //Output
  pinMode(buzzer, OUTPUT);
}

void connectBuzzer() {
  //GND
  pinMode(D6, OUTPUT);
  digitalWrite(D6, LOW);
  //3V3
  pinMode(D7, OUTPUT);
  digitalWrite(D7, HIGH);
}

void disconnectBuzzer() {
  //GND
  pinMode(D6, OUTPUT);
  digitalWrite(D6, HIGH);
  //3V3
  pinMode(D7, OUTPUT);
  digitalWrite(D7, LOW);
}

void makeSound(int note, int duration) {
    tone(buzzer, note, duration + 200);
}

void setup(void) {
  preparePin();
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", []() {
    String noteString = server.arg("note");
    String durationString = server.arg("duration");
    
    if (noteString.length() == 0 || durationString.length() == 0)
      return server.send(400, "text/plain", "Invalid parameters");
    
    double note = server.arg(0).toInt();
    double duration = server.arg(1).toInt();
    
    server.send(200, "text/plain", server.arg(0));

    makeSound(note, duration);
  });

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  connectBuzzer();
  server.handleClient();
  MDNS.update();
}
