#include <ESP8266WiFi.h>
#include <WebSocketClient.h>

const char* ssid     = "gostriksh";
const char* password = "test1234";
char path[] = "/";
char host[] = "192.168.43.186";
  
WebSocketClient webSocketClient;

const int buzzer_signal = D7;
const int buzzer_ground = D6;
const int buzzer_volt = D8;

void preparePin() {
  //Output
  pinMode(buzzer_signal, OUTPUT);
}

void connectBuzzer() {
  //GND
  pinMode(buzzer_ground, OUTPUT);
  digitalWrite(buzzer_ground, LOW);
  //3V3
  pinMode(buzzer_volt, OUTPUT);
  digitalWrite(buzzer_volt, HIGH);
}

void disconnectBuzzer() {
  //GND
  pinMode(buzzer_ground, OUTPUT);
  digitalWrite(buzzer_ground, HIGH);
  //3V3
  pinMode(buzzer_volt, OUTPUT);
  digitalWrite(buzzer_volt, LOW);
}

// Use WiFiClient class to create TCP connections
WiFiClient client;

void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(2000);
  

  // Connect to the websocket server
  if (client.connect("192.168.43.186", 3000)) {
    Serial.println("Connected");
  } else {
    Serial.println("Connection failed.");
    while(1) {
      // Hang on failure
    }
  }

  // Handshake with the server
  webSocketClient.path = path;
  webSocketClient.host = host;
  if (webSocketClient.handshake(client)) {
    Serial.println("Handshake successful");
    webSocketClient.sendData(WiFi.localIP().toString());
  } else {
    Serial.println("Handshake failed.");
    while(1) {
      // Hang on failure
    }  
  }

  connectBuzzer();
  preparePin();
}

void makeSound(int note, int duration) {
    tone(buzzer_signal, note, duration + 200);
}

void loop() {
  String data;

  if (client.connected()) {
    webSocketClient.getData(data);
    if (data.length() > 0) {
      int index = data.indexOf('-');
      if (index != -1) {
        float freq = data.substring(0, index - 1).toFloat();
        int duration = data.substring(index + 1).toInt();
        makeSound(freq, duration); 
      } else {
        webSocketClient.sendData("pong");
      }
    }
    
  } else {
    Serial.println("Client disconnected.");
    while (1) {
      // Hang on disconnect.
    }
  }
}
